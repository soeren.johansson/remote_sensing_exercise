# remote_sensing_exercise


## Description
These scripts aim to illustrate with very simple code snippets how atmospheric spectra can be artificially created and how information retrieval can be performed in a very basic way. This exercise is typically accompanied by a comprehensive lecture, which introduces concepts and theory, which is applied here. In addition, at some parts in the code, equation numbers from the lecture script are provided. It is assumed, that the code itself is not self-explanatory, so it is likely only useful for participants of the course.

## Usage
The scripts are pretty simple and have been tested on an Anaconda installation within the Spyder IDE. In principle, all scripts should work with any python environment, as long as numpy and matplotlib are available.

## Authors and acknowledgment
Implemented by Sören Johansson for the exercise of the remote sensing course given by Thomas von Clarmann at KIT. In case of problems, please contact soeren.johansson@kit.edu. The idea and concept of this exercise is based on the basic scripts provided by Frank Hase.

## License
Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

