# -*- coding: utf-8 -*-
"""
this demo code serves as starting point (offers routine for noise generation + matrix inversion)
1D array (think of a spectrum) is filled with noise
note: we start array indizes with 0
Adapted from the BASIC code provided by Frank Hase.

Constrained retrieval (iterative, nonlinear problem!)
x_neu = x + (KTSy-1K + Sa_inv + LM * 1)-1 [KTSy-1 (ymess - forward(x)) - Sa_inv (x - xa)] # Eqn. (7.38)
deltax = (KTSy-1K + Sa_inv + LM * 1)-1 [KTSy-1 (ymess - forward(x)) - Sa_inv (x - xa)]
Sy-1 is assumed to be not correlated and is represented as unity matrix times sigma^-2
use simple diag choice for Sa (sa_ii) -> Sa_inv diagonal also (sa_inv = 1 / sa_ii)
Now, simulate one gas in different atm levels (line width varies with altitude), 
this time with a Lorentzian line shape
"""
import numpy as np
import matplotlib.pyplot as plt
from demo_inversion_0 import makenoise
from demo_inversion_1 import fwdderi

np.random.seed(1)    # random "seed" value for repeatable noise
plot_spectra = True
plot_state = True

mnue = 50 #number of spectral channels
nlevel = 15 #number of atmospheric levels


xtrue = np.full((nlevel),0.) #true atmospheric state
x = np.full((nlevel),0.) #retrieved atmospheric state

ytrue = np.full((mnue),0.) # true spectrum
y = np.full((mnue),0.) # retrieved spectrum
ymess = np.full((mnue),0.) # measured spectrum
noise = np.full((mnue),0.) # spectral noise

kabs = np.full((mnue, nlevel),0.)

K = np.full((mnue, nlevel), 0.)

xa = np.full((nlevel),0.)


# set spectral characteristics of gases  (now as fct of altitude)
for ilevel in range(nlevel):
    breite = 1.0 * np.sqrt(((ilevel+1) * (ilevel+1))) + 1.0
    norm = 2.0 * breite
    for inue in range(mnue):
        dnue = float(inue+1) - 0.5 * float(mnue)
        kabs[inue, ilevel] = norm / (dnue * dnue + breite * breite)
# print(kabs)

file = open('kabs.dat','w')
for inue in range(mnue):
    for ilevel in range(nlevel):
        file.write("  {:2.4f}".format(kabs[inue, ilevel]))
    file.write('\n')
file.close()

# set true atmospheric state
# set true atmospheric state
# xtrue[:] = .25 # for all altitudes
xtrue[0] = .25
xtrue[1] = .5
xtrue[2] = .75
xtrue[3] = 1.
xtrue[4] = 1.35
xtrue[5] = 1.25
xtrue[6] = .95
xtrue[7] = .75
xtrue[8] = .5
xtrue[9] = .25
xtrue[10] = .15
xtrue[11] = .05

gradtorad = np.pi / 180.0
sza = gradtorad * 0.0 #SZA

# forward model run
fwdderi(nlevel, mnue, kabs, sza, xtrue, ytrue, K)

 # simulate measured spectrum
sigma = 0.01
noise = makenoise(mnue, sigma)
for inue in range(mnue):
    ymess[inue] = ytrue[inue] + noise[inue]

#simulated spectrum and jacobian as output
file = open("jak.dat",'w')
for inue in range(mnue):
    file.write(" {:2.3f}".format(ymess[inue]))
    file.write('  ')
    for ix in range(nlevel):
        file.write(" {:2.3f}".format(K[inue, ix]))
    file.write('\n')
file.close()


# retrieval section
# first guess
x[:] = .05 # for all altitudes

# a-priori
xa[:] = .25 # for all altitudes
# xa[0] = 0.
# xa[1] = .25
# xa[2] = .5
# xa[3] = 1.
# xa[4] = 1.25
# xa[5] = .5
# xa[11] = .5

# Sa_inv (diagonal)
Sa_inv = np.identity(nlevel)
for ilevel in range(nlevel):
    Sa_inv[ilevel,ilevel] = 0.6#1 * 1 / (0.99 * xa[ilevel] * xa[ilevel]) #50% 1 sig variability of clim value expected
    # Use fixed assumed noise for Sa, instead of sigma * sigma


n_iterations = 5 #number of retrieval iterations
LM = 0.0 #Levenberg-Marquard term

if plot_spectra:
    fig_spectra = plt.figure()
    fig_spectra_diff = plt.figure()
    # plt.plot(ymess,'k-',label='meas')

if plot_state:
    fig_state = plt.figure()
    plt.plot(x,np.arange(len(x)),'x-',label='initial guess')
    plt.plot(xa,np.arange(len(xa)),'x-',label='a priori')

for i in range(n_iterations):
    print("state vector before iteration:")
    print(x)

    # x_neu = x + (KTSy-1K + Sa_inv + LM * 1)-1 [KTSy-1 (ymess - forward(x)) - Sa_inv (x - xa)] # Eqn. (7.38)
    # deltax = (KTSy-1K + Sa_inv + LM * 1)-1 [KTSy-1 (ymess - forward(x)) - Sa_inv (x - xa)]
    fwdderi(nlevel, mnue, kabs, sza, x, y, K)
    # deltay = ymess - forward(x)
    deltay = ymess-y
    # KTSy-1dy = KTSy-1 (ymess - forward(x)) - Sa_inv (x - xa)]
    KTSydy = np.dot(K.T/sigma/sigma,deltay) - np.dot(Sa_inv,(x-xa))

    # KTSy-1K approximated with KTK*sigma^-2
    KTSyK = np.dot(K.T,K)/sigma/sigma


    # KTK -> KTK + Sa_inv + LM * 1
    KTSyK = KTSyK + Sa_inv + LM * np.identity(len(KTSyK))

    # matrix inversion
    KTSyKinv = np.linalg.inv(KTSyK)
    # deltax = KTSy-1Kinv * KTSy-1dy
    deltax = np.dot(KTSyKinv,KTSydy)

    # update state vector
    for ix in range(nlevel):
        x[ix] = x[ix] + deltax[ix]
    if plot_state:
        plt.figure(fig_state.number)
        plt.plot(x,np.arange(len(x)),'x-',label='iteration '+str(i))
    if plot_spectra:
        plt.figure(fig_spectra.number)
        plt.plot(y,label='fit iteration '+str(i))
        plt.figure(fig_spectra_diff.number)
        plt.plot(ymess-y,label='fit iteration '+str(i))
if plot_state:
    plt.figure(fig_state.number)
    plt.plot(xtrue,np.arange(len(x)),'kx:',label='true state')
    plt.title('Atmospheric state')
    plt.xlabel('Gas concentration')
    plt.ylabel('level no.')
    plt.yticks(np.arange(nlevel),[str(int(l)) for l in np.arange(nlevel)])
    plt.legend()
    plt.savefig('demo_inversion_6-lorentzian_state.png',dpi=150)
#    plt.close(fig_state.number)

if plot_spectra:
    plt.figure(fig_spectra.number)
    plt.plot(ymess,'k-',label='meas')

    fwdderi(nlevel, mnue, kabs, sza, x, y,K)

    plt.plot(y,'r--',label='final fit')
    plt.title('Spectra')
    plt.xlabel('Spectral grid')
    plt.ylabel('Transmission')
    plt.legend()
    plt.savefig('demo_inversion_6-lorentzian_spectra.png',dpi=150)
#    plt.close(fig_spectra.number)

    plt.figure(fig_spectra_diff.number)
    plt.plot(ymess-y,'r--',label='final fit')
    plt.plot(ymess*0.,'k-',zorder=0)
    plt.title('Spectra residuals')
    plt.xlabel('Spectral grid')
    plt.ylabel('$\Delta$ Transmission')
    plt.legend()
    plt.savefig('demo_inversion_6-lorentzian_spectra_diff.png',dpi=150)
#    plt.close(fig_spectra_diff.number)

file = open("profile.dat",'w')
for ilevel in range(nlevel):
    file.write(" {:2.4f}".format(x[ilevel]))
    file.write('\n')
file.close()
