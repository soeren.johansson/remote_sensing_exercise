# -*- coding: utf-8 -*-
"""
this demo code serves as starting point (offers routine for noise generation + matrix inversion)
1D array (think of a spectrum) is filled with noise
note: we start array indizes with 0
Adapted from the BASIC code provided by Frank Hase.

Example 2: more spectral gridpoints
"""

import numpy as np
import matplotlib.pyplot as plt
from demo_inversion_0 import makenoise
from demo_inversion_1 import fwdderi

def main():
    plot_spectra = True

    nnue = 10 #number of spectral channels
    ngas = 3 #number of gases

    xtrue = np.full((ngas),0.) #true atmospheric state
    x = np.full((ngas),0.) #retrieved atmospheric state
    ytrue = np.full((nnue),0.) # true spectrum
    y = np.full((nnue),0.) # retrieved spectrum
    ymess = np.full((nnue),0.) # measured spectrum
    noise = np.full((nnue),0.) # spectral noise
    
    kabs = np.full((nnue, ngas),0.)
    
    K = np.full((nnue, ngas), 0.)
    
    # set spectral characteristics of gases
    # gas 0
    kabs[1, 0] = 1.0
    # gas 1
    kabs[4, 1] = 0.1
    # gas 2
    kabs[7, 2] = 0.5
    
    # set true atmospheric state
    xtrue[0] = 0.1 #gas 0
    xtrue[1] = 0.1 #gas 1
    xtrue[2] = 0.1 #gas 1
    gradtorad = np.pi / 180.0
    sza = gradtorad * 0.0 #SZA
    
    
    # forward model run
    #forward(ngas, nnue, kabs, sza, xtrue, ytrue)
    fwdderi(ngas, nnue, kabs, sza, xtrue, ytrue, K)
    
     # simulate measured spectrum
    sigma = 0.01
    noise = makenoise(nnue, sigma)
    for inue in range(nnue):
        ymess[inue] = ytrue[inue] + noise[inue]

    print("Simulated spectrum:")
    print(ymess)
    print(ytrue)

    print("Jacobean:")
    print(K)
    if plot_spectra:
        plt.plot(ymess,label='ymess')
        plt.plot(ytrue,label='ytrue')
        plt.title('Spectra')
        plt.xlabel('Spectral grid')
        plt.ylabel('Transmission')
        plt.legend()
        plt.savefig('demo_inversion_2_spectra.png',dpi=150)

if __name__ == "__main__":
    main()