 # -*- coding: utf-8 -*-
"""
this demo code serves as starting point (offers routine for noise generation + matrix inversion)
1D array (think of a spectrum) is filled with noise
note: we start array indizes with 0
Adapted from the BASIC code provided by Frank Hase.

Unconstrained retrieval (iterative, nonlinear problem!)
include LM for stabilization (needed if gas 1 first guess near saturation)
x_neu = x + (KTSy-1K + LM * 1)-1 KTSy-1 (ymess - forward(x)) # Eqn.(7.24)
deltax = (KTSy-1K + LM * 1)-1 KTSy-1 (ymess - forward(x))
Sy-1 is assumed to be not correlated and is represented as unity matrix times sigma^-2
"""
import numpy as np
import matplotlib.pyplot as plt
from demo_inversion_0 import makenoise
from demo_inversion_1 import fwdderi

np.random.seed(1)    # random "seed" value for repeatable noise
plot_spectra = True
plot_state = True

mnue = 10 #number of spectral channels
ngas = 2 #number of gases

xtrue = np.full((ngas),0.) #true atmospheric state
x = np.full((ngas),0.) #retrieved atmospheric state

ytrue = np.full((mnue),0.) # true spectrum
y = np.full((mnue),0.) # retrieved spectrum
ymess = np.full((mnue),0.) # measured spectrum
noise = np.full((mnue),0.) # spectral noise

kabs = np.full((mnue, ngas),0.)

K = np.full((mnue, ngas), 0.)




# set spectral characteristics of gases
# gas 0
kabs[1, 0] = 1.1
# gas 1
kabs[4, 1] = 1.1


# set true atmospheric state
xtrue[0] = 1.0 #gas 0
xtrue[1] = 0.9 #gas 1

gradtorad = np.pi / 180.0
sza = gradtorad * 0.0 #SZA

# forward model run
#forward(ngas, mnue, kabs, sza, xtrue, ytrue)
fwdderi(ngas, mnue, kabs, sza, xtrue, ytrue, K)

 # simulate measured spectrum
sigma = 0.01
noise = makenoise(mnue, sigma)
for inue in range(mnue):
    ymess[inue] = ytrue[inue] + noise[inue]

print("Simulated spectrum:")
print(ymess)

print("Jacobean:")
print(K)


# retrieval section
# first guess
x[0] = 0.0
x[1] = 0.0

n_iterations = 5 #number of retrieval iterations
LM = .4 #Levenberg-Marquard term


if plot_spectra:
    fig_spectra = plt.figure()
    fig_spectra_diff = plt.figure()

if plot_state:
    fig_state = plt.figure()
    plt.plot(np.arange(len(x)),x,'x-',label='initial guess')


for i in range(n_iterations):
    print("state vector before iteration:")
    print(x)

    # x_neu = x + (KTSy-1K + LM * 1)-1 KTSy-1 (ymess - forward(x)) # Eqn.(7.24)
    # deltax = (KTSy-1K + LM * 1)-1 KTSy-1 (ymess - forward(x))
    fwdderi(ngas, mnue, kabs, sza, x, y, K)
    # deltay = ymess - forward(x)
    deltay = ymess-y
    # KTSy-1dy = KTSy-1 (ymess - forward(x))
    KTSydy = np.dot(K.T/sigma/sigma,deltay)

    # KTSy-1K approximated with KTK*sigma^-2
    KTSyK = np.dot(K.T,K)/sigma/sigma

    # Levenberg-Marquardt
    # KTSy-1K -> KTSy-1K + LM * 1
    KTSyK = KTSyK+ LM * np.identity(len(KTSyK))

    # matrix inversion
    KTSyKinv = np.linalg.inv(KTSyK)
    # deltax = KTSy-1Kinv * KTSy-1dy
    deltax = np.dot(KTSyKinv,KTSydy)

    # update state vector
    for ix in range(ngas):
        x[ix] = x[ix] + deltax[ix]
    if plot_state:
        plt.figure(fig_state.number)
        plt.plot(np.arange(len(x)),x,'x-',label='iteration '+str(i))
    if plot_spectra:
        plt.figure(fig_spectra.number)
        plt.plot(y,label='fit iteration '+str(i))
        plt.figure(fig_spectra_diff.number)
        plt.plot(ymess-y,label='fit iteration '+str(i))
if plot_state:
    plt.figure(fig_state.number)
    plt.plot(np.arange(len(x)),xtrue,'kx:',label='true state')
    plt.title('Atmospheric state')
    plt.ylabel('Gas concentration')
    plt.xlabel('Gas no.')
    plt.xticks(np.arange(ngas),[str(int(l)) for l in np.arange(ngas)])
    plt.legend()
    plt.savefig('demo_inversion_4_state.png',dpi=150)
#    plt.close(fig_state.number)

if plot_spectra:
    plt.figure(fig_spectra.number)
    plt.plot(ymess,'k-',label='meas')

    fwdderi(ngas, mnue, kabs, sza, x, y,K)

    plt.plot(y,'r--',label='final fit')
    plt.title('Spectra')
    plt.xlabel('Spectral grid')
    plt.ylabel('Transmission')
    plt.legend()
    plt.savefig('demo_inversion_4_spectra.png',dpi=150)
#    plt.close(fig_spectra.number)

    plt.figure(fig_spectra_diff.number)
    plt.plot(ymess-y,'r--',label='final fit')
    plt.plot(ymess*0.,'k-',zorder=0)
    plt.title('Spectra residuals')
    plt.xlabel('Spectral grid')
    plt.ylabel('$\Delta$ Transmission')
    plt.legend()
    plt.savefig('demo_inversion_4_spectra_diff.png',dpi=150)
#    plt.close(fig_spectra_diff.number)
