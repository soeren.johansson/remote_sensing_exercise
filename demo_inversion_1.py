# -*- coding: utf-8 -*-
"""
This demo code serves as starting point (offers routine for noise generation + matrix inversion)
1D array (think of a spectrum) is filled with noise
note: we start array indizes with 0
Adapted from the BASIC code provided by Frank Hase.

Example 1: we create a simplistic forward model (gb-solar absorption like): transm(nue) = norm * exp(-kabs(nue) * x_gas / cos(SZA)))
"""
import numpy as np
from demo_inversion_0 import makenoise
import matplotlib.pyplot as plt

 # TODO : Nummern von Gleichungen im Skript, kurze Einführung...

def forward(ngas, nnue, kabs, sza, x, y):
    """
    Simple forward model for solar absorption

    Parameters
    ----------
    ngas : int
        Number of gases that are considered.
    nnue : int
        number of spectral points that are considered.
    kabs : numpy array of floats in shape (nnue, ngas)
        Absorption coefficient array for each spectral point and each gas.
    sza : float
        Solar zenith angle in rad.
    x : numpy array of floats
        Gas column.
    y : numpy array of floats
        Vector in which the synthetic spectrum is written.

    Returns
    -------
    None.

    """
    for inue in range(nnue):
        sumtau = 0.
        for igas in range(ngas):
            sumtau += kabs[inue, igas]*x[igas] # exponent of eq. 3.2, where kabs can be regarded as a matrix representation of sigma
        y[inue] = np.exp(-sumtau/np.cos(sza)) # first part of eq. 3.1, but also with SZA considered


def fwdderi(ngas, nnue, kabs, sza, x, y, K):
    """
    Simple forward model for solar absorption incl. calculation of Jacobean

    Parameters
    ----------
    ngas : int
        Number of gases that are considered.
    nnue : int
        number of spectral points that are considered.
    kabs : numpy array of floats in shape (nnue, ngas)
        Absorption coefficient array for each spectral point and each gas.
    sza : float
        Solar zenith angle in rad.
    x : numpy array of floats
        Gas column.
    y : numpy array of floats
        Vector in which the synthetic spectrum is written.
    K : numpy array of floats
        Matrix in which the Jacobean is written.        

    Returns
    -------
    None.

    """
    for inue in range(nnue):
        sumtau = 0.
        for igas in range(ngas):
            sumtau += kabs[inue,igas] * x[igas] # exponent of eq. 3.2, where kabs can be regarded as a matrix representation of sigma
        y[inue] = np.exp(-sumtau / np.cos(sza)) # first part of eq. 3.1, but also with SZA considered

        for igas in range(ngas):
            K[inue,igas] = -y[inue] * kabs[inue,igas] / np.cos(sza) # dy/dx for each element


# Main Programm starts here
def main():
    plot_spectra = True

    nnue = 5 #number of spectral channels
    ngas = 2 #number of gases

    xtrue = np.full((ngas),0.) #true atmospheric state
    x = np.full((ngas),0.) #retrieved atmospheric state
    ytrue = np.full((nnue),0.) # true spectrum
    y = np.full((nnue),0.) # retrieved spectrum
    ymess = np.full((nnue),0.) # measured spectrum
    noise = np.full((nnue),0.) # spectral noise
    
    kabs = np.full((nnue, ngas),0.)
    
    K = np.full((nnue, ngas), 0.)
    
    # set spectral characteristics of gases
    # gas 0
    kabs[1, 0] = 1.0
    kabs[4, 0] = 1.0
    # gas 1
    kabs[4, 1] = 1.0
    
    print(kabs)
    
    # set true atmospheric state
    xtrue[0] = 0.9 #gas 0
    xtrue[1] = 0.9 #gas 1
    gradtorad = np.pi / 180.0
    sza = gradtorad * 0.0 #SZA
    
    # forward model run
    # forward(ngas, nnue, kabs, sza, xtrue, ytrue)
    fwdderi(ngas, nnue, kabs, sza, xtrue, ytrue, K)
    
    # simulate measured spectrum
    sigma = 0.02
    noise = makenoise(nnue, sigma)
    for inue in range(nnue):
        ymess[inue] = ytrue[inue] + noise[inue]
    
    print("Simulated spectrum:")
    print(ymess)
    print(ytrue)

    print("Jacobean:")
    print(K)
    if plot_spectra:
        plt.plot(ymess,label='ymess')
        plt.plot(ytrue,label='ytrue')
        plt.legend()
        plt.savefig('demo_inversion_1_spectra.png',dpi=150)

if __name__ == "__main__":
    main()
