# -*- coding: utf-8 -*-
"""
This demo code serves as starting point (offers routine for noise generation + matrix inversion)
1D array (think of a spectrum) is filled with noise
note: we start array indizes with 0
Adapted from the BASIC code provided by Frank Hase.
"""

import numpy as np

def gaussnoise(sigma):
    """
    generates Gaussian noise (Box-Muller method)

    Parameters
    ----------
    sigma : float
        Scaling factor of the white noise.

    Returns
    -------
    float
        Gaussian noise according to the Box-Muller method.

    """
    return sigma*np.sqrt(-2. * np.log(np.random.random())) * np.cos(2. * np.pi * np.random.random())

def makenoise(nmax,sigma):
    """
    fills Gaussian noise into an array

    Parameters
    ----------
    nmax : int
        Length of noise array to be generated.
    sigma : float
        Sigma parameter given to the Gaussian noise generator.

    Returns
    -------
    numpy array 
        array filled with Gaussian noise

    """
    return np.array([gaussnoise(sigma) for i in range(0,nmax)])

def matinvers(matein):
    """
    Gauss-Jordan matrix inversion. This is only here for illustrative purposes. 
    Later, the built-in numpy function will be used.

    Parameters
    ----------
    matein : numpy array
        Matrix to be inversed

    Returns
    -------
    matinv : numpy array
        Inversed matrix

    """
    nmax = matein.shape[0]
    if nmax != matein.shape[1]: raise Exception('The number of columns and rows of the input matrix are not identical!')
    matwrk = np.full((nmax,2*nmax),0., dtype='f')
    matinv = np.full((nmax,nmax),np.nan, dtype='f')

    # Duplicate input matrix in first half of work matrix
    for i in range(nmax):
        for j in range(nmax):
            matwrk[i,j] = matein[i,j]
    # Make diagonal elements of second half of work matrix equal 1
    for i in range(nmax):
        matwrk[i,nmax+i] = 1.

    for i in range(nmax):
        for j in range (nmax):
            if i != j:
                ratio = matwrk[j,i] / matwrk[i,i]
                for k in range(2*nmax):
                    matwrk[j,k] = matwrk[j,k] - ratio * matwrk[i,k]

    for i in range(nmax):
        normwert = matwrk[i,i]
        for j in range(nmax,2*nmax):
            matwrk[i,j] = matwrk[i,j] / normwert

    for i in range(nmax):
        for j in range(nmax):
            matinv[i,j] = matwrk[i,nmax+j]

    return matinv

# Main program starts here!
def main():
    nnue = 50
    
    noise = makenoise(nnue, 1.)
    
    print(noise)
    
    matrixin = np.array([[10,1,1],
                          [0,1,0],
                          [0,5,1]])
    print(matrixin)
    print(matinvers(matrixin))
    print(np.linalg.inv(matrixin)) #built in numpy function to check if the self-made matinvers function is working properly


if __name__ == "__main__":
    main()
